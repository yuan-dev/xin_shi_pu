import Vue from 'vue'
import VueRouter from 'vue-router'
import shouye from '../views/ShouYe.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'home',
        component: shouye
    },
    {
        path: '/xq',
        name: 'routeName',
        component: () =>
            import ('../views/RecipeXq.vue'),
    },
    {
        path: '/userxx',
        name: 'routeName',
        component: () =>
            import ('../views/UserXx.vue'),
    },
    {
        path: '/recipelb',
        name: 'routeName',
        component: () =>
            import ('../views/RecipeLb.vue'),
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router